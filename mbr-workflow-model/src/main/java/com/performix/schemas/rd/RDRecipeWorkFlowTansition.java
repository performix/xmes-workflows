/**
 * 
 */
package com.performix.schemas.rd;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Performix
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RDRecipeWorkFlowTansition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String id;
	private String recipeStateTransitionId;
	private String profile;
	private String comments;
	private String userId;
	private Boolean reviewCompleted;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the recipeStateTransitionId
	 */
	public String getRecipeStateTransitionId() {
		return recipeStateTransitionId;
	}
	/**
	 * @param recipeStateTransitionId the recipeStateTransitionId to set
	 */
	public void setRecipeStateTransitionId(String recipeStateTransitionId) {
		this.recipeStateTransitionId = recipeStateTransitionId;
	}
	/**
	 * @return the profile
	 */
	public String getProfile() {
		return profile;
	}
	/**
	 * @param profile the profile to set
	 */
	public void setProfile(String profile) {
		this.profile = profile;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	/**
	 * @return the reviewCompleted
	 */
	public Boolean getReviewCompleted() {
		return reviewCompleted;
	}
	/**
	 * @param reviewCompleted the reviewCompleted to set
	 */
	public void setReviewCompleted(Boolean reviewCompleted) {
		this.reviewCompleted = reviewCompleted;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RDRecipeWorkFlowTansition [recipeStateTransitionId=" + recipeStateTransitionId + ", profile=" + profile
				+ ", comments=" + comments + ", userId=" + userId + ", reviewCompleted=" + reviewCompleted + "]";
	}

}
