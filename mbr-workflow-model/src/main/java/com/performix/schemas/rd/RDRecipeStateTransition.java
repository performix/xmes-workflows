/**
 * 
 */
package com.performix.schemas.rd;

import java.io.Serializable;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * @author Performix
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RDRecipeStateTransition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	   
    private java.util.List<com.performix.schemas.rd.RDRecipeWorkFlowTansition> recipeWorkFlowTansitions;
   
    private String templateVersionId;
  
    private String fromState;
    
    private String toState;
    
    private String comments;
    
    private String userId;
          
    private Boolean wfProcessDone;
    
    private String plantId;
    
    private String enterpriseID;
    
    private String processInstanceId;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the recipeWorkFlowTansitions
	 */
	public java.util.List<com.performix.schemas.rd.RDRecipeWorkFlowTansition> getRecipeWorkFlowTansitions() {
		if(this.recipeWorkFlowTansitions == null) {
			return new ArrayList<com.performix.schemas.rd.RDRecipeWorkFlowTansition>();
		}
		return recipeWorkFlowTansitions;
	}

	/**
	 * @param recipeWorkFlowTansitions the recipeWorkFlowTansitions to set
	 */
	public void setRecipeWorkFlowTansitions(
			java.util.List<com.performix.schemas.rd.RDRecipeWorkFlowTansition> recipeWorkFlowTansitions) {
		this.recipeWorkFlowTansitions = recipeWorkFlowTansitions;
	}

	/**
	 * @return the templateVersionId
	 */
	public String getTemplateVersionId() {
		return templateVersionId;
	}

	/**
	 * @param templateVersionId the templateVersionId to set
	 */
	public void setTemplateVersionId(String templateVersionId) {
		this.templateVersionId = templateVersionId;
	}

	/**
	 * @return the fromState
	 */
	public String getFromState() {
		return fromState;
	}

	/**
	 * @param fromState the fromState to set
	 */
	public void setFromState(String fromState) {
		this.fromState = fromState;
	}

	/**
	 * @return the toState
	 */
	public String getToState() {
		return toState;
	}

	/**
	 * @param toState the toState to set
	 */
	public void setToState(String toState) {
		this.toState = toState;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the wfProcessDone
	 */
	public Boolean getWfProcessDone() {
		return wfProcessDone;
	}

	/**
	 * @param wfProcessDone the wfProcessDone to set
	 */
	public void setWfProcessDone(Boolean wfProcessDone) {
		this.wfProcessDone = wfProcessDone;
	}

	/**
	 * @return the plantId
	 */
	public String getPlantId() {
		return plantId;
	}

	/**
	 * @param plantId the plantId to set
	 */
	public void setPlantId(String plantId) {
		this.plantId = plantId;
	}

	/**
	 * @return the enterpriseID
	 */
	public String getEnterpriseID() {
		return enterpriseID;
	}

	/**
	 * @param enterpriseID the enterpriseID to set
	 */
	public void setEnterpriseID(String enterpriseID) {
		this.enterpriseID = enterpriseID;
	}

	/**
	 * @return the processInstanceId
	 */
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	/**
	 * @param processInstanceId the processInstanceId to set
	 */
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RDRecipeStateTransition [recipeWorkFlowTansitions=" + recipeWorkFlowTansitions + ", templateVersionId="
				+ templateVersionId + ", fromState=" + fromState + ", toState=" + toState + ", comments=" + comments
				+ ", userId=" + userId + ", wfProcessDone=" + wfProcessDone + ", plantId=" + plantId + ", enterpriseID="
				+ enterpriseID + ", processInstanceId=" + processInstanceId + "]";
	}
   
	
}
